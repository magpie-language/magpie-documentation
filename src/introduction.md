![Magpie Logo](https://world-of-music.at/downloads/magpie-banner2.png)

# Introduction

---

Magpie is an optionally typed, object-oriented programming language with pattern matching, multiple dispatch and a syntax that can be extended at runtime. If you are confused by these terms, don't worry, we'll explain each of them in detail later on.

Here's what the code for a (simple and very inefficient) fibonacci function could look like:

```
def fib(0) 0
def fib(1) 1
def fib(n is Int) fib(n - 2) + fib(n - 1)
```

Note that this method takes a *single* parameter, namely a record with two fields: `name` and `pals`. The former is a `String` which is then bound to the `n` variable inside the method, and the latter is a tuple with two items.
## Features

### **Pattern Matching**

Patterns are an integral part of the syntax and can be found everywhere in the language.

### **Multiple Dispatch**

The nature of multiple dispatch allows you to assign more than one function to a single name, as long as they have different parameters. This might sound strange at first, but it makes a lot of sense when you actually think about it. Consider jQuery's dollar function:

TODO

## A simple "Hello, World" program

A simple Hello World program can be as small as this:

```
print('Hello World!')
```

Usually, though, it is recommended to wrap the code in a main function, so type-checking is enabled within the compiler:

```
def main()
    print('Hello World!')
end
```